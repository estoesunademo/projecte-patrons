USE `videojoc`;

CREATE OR REPLACE TABLE `jugador` (
  `idjugador` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `saldo` int(11) DEFAULT 0,
  `punts` int(11) DEFAULT 0,
  PRIMARY KEY (`idjugador`)
);