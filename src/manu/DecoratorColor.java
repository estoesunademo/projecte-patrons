package manu;

public class DecoratorColor extends Player_Decorator 
{
	private ColorEnum color;

	public DecoratorColor(Player player, ColorEnum color) {
		super(player);
		this.color = color;
	}

	@Override
	public String getDescription() {

		return super.getDescription() + ", soy de color " + this.color;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setId(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPoints() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getBalance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setPoints(int points) {
		// TODO Auto-generated method stub

	}
	

	@Override
	public String toString() {
		return "DecoratorColor [color=" + color + "]";
	}

	@Override
	public int compareTo(Player o) {
		return 0;
	}

}
