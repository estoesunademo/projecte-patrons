package manu;

import marti.Vehicle;

public class DecoratorVehicle extends Player_Decorator {

	private Vehicle vehicle;

	public DecoratorVehicle(Player player, Vehicle vehicle) 
	{
		super(player);
		this.vehicle = vehicle;
	}

	@Override
	public String getDescription() {
		return super.getDescription() + ", tengo un vehículo "+vehicle.getName();
	}
	
	
	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setId(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPoints() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getBalance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setPoints(int points) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String toString() {
		return "DecoratorVehicle [vehicle=" + vehicle + "]";
	}

	@Override
	public int compareTo(Player o) {
		return 0;
	}

}
