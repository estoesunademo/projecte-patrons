package manu;

public interface Player extends Comparable<Player> {
	
	public int getId();
	public void setId(int id);
	public String getName();
	public int getPoints();
	public int getBalance();
	public void setPoints(int points);
	
	public String getDescription();
	
	
}
