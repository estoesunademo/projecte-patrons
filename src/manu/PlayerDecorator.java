package manu;

public abstract class PlayerDecorator implements Player
{
	protected Player playerToDecorate;
	
	public PlayerDecorator(Player player) {
		this.playerToDecorate = player;
	}
	
	@Override
	public String getDescription() {
		return "..";
	}
	
	
}
