package manu;

public abstract class Player_Decorator implements Player
{
	protected Player playerToDecorate;
	
	public Player_Decorator(Player player) {
		this.playerToDecorate = player;
	}

	public Player getPlayerToDecorate() {
		return playerToDecorate;
	}

	@Override
	public String getDescription() {
		return playerToDecorate.getDescription();
	}
	
	
}
