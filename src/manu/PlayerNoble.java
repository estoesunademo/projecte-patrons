package manu;

public class PlayerNoble implements Player{

	private int id;
	private String name;
	private int points;
	private int balance;
	
	
	public PlayerNoble(int id, String name, int points, int balance) {
		super();
		this.id = id;
		this.name = name;
		this.points = points;
		this.balance = balance;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int getPoints() {
		return this.points;
	}

	@Override
	public int getBalance() {
		return this.balance;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setPoints(int points) {
		this.points = points;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "PlayerNoble [name=" + name + ", points=" + points + ", balance=" + balance + "]";
	}
	@Override
	public String getDescription() {
		return "Soy un noble";
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public int compareTo(Player o) {
		return this.getName().compareTo(o.getName());
	}
}
