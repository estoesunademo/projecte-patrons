package manu;

import java.util.ArrayDeque;
import java.util.Deque;

import marti.ObjecteEnum;
import marti.Upgrade;
import marti.Vehicle;
import marti.AbstractFactory;
import marti.FactoryVehicle;
import marti.Objecte;
import marti.AFactoryProvider;
import marti.FactoryUpgrade;
import marti.FactoryCollectable;

public class MainManu {

	public static void main(String[] args) {
/*
		// Utilitza Factory Method per crear els jugadors de l’aplicació
		Player_Factory factory = new Player_Factory();

		// Aquest jugadors s’han de guardar en una pila.
		Deque<Player> playerList = new ArrayDeque<>();

		Player pl01 = factory.createPlayer(TypePlayer.WARRIOR, 1, "Goliath", 0, playerList);
		Player pl02 = factory.createPlayer(TypePlayer.MERCHANT, 2, "Kitsune", 0, playerList);
		Player pl03 = factory.createPlayer(TypePlayer.NOBLE, 3, "Griffith", 0, playerList);

		// Mostra per pantalla la creació dels jugadors.
		for (Player player : playerList) {
			System.out.println(player);
		}
*/
		
		// 04 DECORATORS  --------------->
		
		// Utilitza Factory Method per crear els jugadors de l’aplicació
		Player_Factory factory = new Player_Factory();

		// Aquest jugadors s’han de guardar en una pila.
		Deque<Player> playerList = new ArrayDeque<>();
		
		Player pl04 = factory.createPlayer(TypePlayer.WARRIOR, 4, "Xena", 0, playerList);
		
		AFactoryProvider factoryP = new AFactoryProvider();
		
		Vehicle cotxe = (Vehicle) factoryP.getFactory(ObjecteEnum.CAR).generate("Volvo XC60");
		Upgrade up = (Upgrade) factoryP.getFactory(ObjecteEnum.LAXANT).generate("Laxante");
		Color rojo = new Color(ColorEnum.HOTPINK);
		
		System.out.println(cotxe);
		
		pl04 = new DecoratorVehicle(pl04, cotxe);
		pl04 = new DecoratorUpgrade(pl04, up);
		pl04 = new DecoratorColor(pl04, ColorEnum.LIME);
		
		System.out.println(pl04.getDescription());
		
//		Pizza p69 = new MassaTradicional();
//		p69 = new Tomate(p69);
//		p69 = new Mozzarella(p69);
		
		
	}
}
