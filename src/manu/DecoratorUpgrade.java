package manu;

import marti.Upgrade;

public class DecoratorUpgrade extends Player_Decorator
{

	private Upgrade upgrade;
	
	public DecoratorUpgrade(Player player, Upgrade upgrade) {
		super(player);
		this.upgrade = upgrade;
	}
	
	
	@Override
	public String getDescription() {
		return super.getDescription() + ", tengo un " + upgrade.getName();
	}
	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setId(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPoints() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getBalance() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setPoints(int points) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String toString() {
		return "DecoratorUpgrade [upgrade=" + upgrade + "]";
	}


	@Override
	public int compareTo(Player o) {
		return 0;
	}

}
