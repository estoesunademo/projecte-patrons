package manu;

import java.util.ArrayDeque;
import java.util.Deque;

public class Player_Factory {
	
	Deque<Player> players = new ArrayDeque<>();

	public Player createPlayer(TypePlayer type, int id, String name, int balance, Deque<Player> players) {
		
		Player newPlayer = null;
		
		if(type == TypePlayer.MERCHANT) newPlayer = new PlayerMerchant(id, name, balance, balance);
		else if (type == TypePlayer.WARRIOR) newPlayer = new PlayerWarrior(id, name, balance, balance);
		else if (type == TypePlayer.NOBLE) newPlayer = new PlayerNoble(id, name, balance, balance);
		
		players.push(newPlayer);
		return newPlayer;
				
	}
	
public Player createPlayer(TypePlayer type, int id, String name, int balance) {
		
		Player newPlayer = null;
		
		if(type == TypePlayer.MERCHANT) newPlayer = new PlayerMerchant(id, name, balance, balance);
		else if (type == TypePlayer.WARRIOR) newPlayer = new PlayerWarrior(id, name, balance, balance);
		else if (type == TypePlayer.NOBLE) newPlayer = new PlayerNoble(id, name, balance, balance);
		
		return newPlayer;
				
	}
}
