package ex6;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import manu.Player;
import manu.PlayerWarrior;

public class DBHandler {

	private String db_conn;

	private Connection conn;
	private Statement stmt;
	private ResultSet rs;

	public DBHandler(String db_host, String db_name, String db_user, String db_pass) {
		super();

		conn = null;
		stmt = null;
		rs = null;

		db_conn = "jdbc:mysql://" + db_host + "/" + db_name + "?" + "user=" + db_user + "&password=" + db_pass;
	}

	public boolean connect() {
		try {
			conn = DriverManager.getConnection(db_conn);
			stmt = conn.createStatement();
			return true;
		} catch (SQLException e) {
			sql_exception(e);
			return false;
		}
	}

	public void close() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException sqlEx) {
			}
			rs = null;
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException sqlEx) {
			}
			stmt = null;
		}
	}

	public List<Player> get_players() {
		List<Player> players = new ArrayList<Player>();
		try {
			if (stmt.execute("SELECT * FROM jugador")) {
				rs = stmt.getResultSet();
				if (rs == null)
					return players;

				while (rs.next()) {
					int id = rs.getInt("idjugador");
					String name = rs.getString("nom");
					int balance = rs.getInt("saldo");
					int points = rs.getInt("punts");
					Player p = new PlayerWarrior(id, name, points, balance);
					players.add(p);
				}
			}
		} catch (SQLException e) {
			sql_exception(e);
		}
		return players;
	}

	public boolean write_player(Player p) {
		String insert = "INSERT INTO jugador (idjugador, nom, saldo, punts) ";
		try {
			String values = "VALUES ('" + p.getId() + "', '" + p.getName() + "', '" + p.getBalance() + "', '"
					+ p.getPoints() + "')";
			String handler = "ON DUPLICATE KEY UPDATE idjugador=" + p.getId() + ", nom='" + p.getName() + "', saldo='"
					+ p.getBalance() + "', punts='" + p.getPoints() + "';";
			String sql = insert + values + handler;
			stmt.execute(sql);
			return true;
		} catch (SQLException e) {
			sql_exception(e);
			return false;
		}
	}

	public boolean delete_player(Player p) {
		String sql = "DELETE FROM jugador WHERE idjugador=" + p.getId();
		try {
			stmt.execute(sql);
			return true;
		} catch (SQLException e) {
			sql_exception(e);
			return false;
		}
	}

	private void sql_exception(SQLException e) {
		System.out.println("SQLException: " + e.getMessage());
		System.out.println("SQLState: " + e.getSQLState());
		System.out.println("VendorError: " + e.getErrorCode());
	}

}
