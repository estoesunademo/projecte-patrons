package ex6;

import java.util.Comparator;
import java.util.List;

import manu.Player;
import manu.PlayerWarrior;


public class MainEx6 {

    public static void main(String[] args) {
        String db_host = "localhost";
        String db_name = "videojocjdbc";
        String db_user = "root";
        String db_pass = "super3";
        
        DBHandler db = new DBHandler(db_host, db_name, db_user, db_pass);
        
        db.connect();

        List<Player> players = db.get_players();

//        players.add(new PlayerWarrior(1, "pepeta", 400, 40));
//        players.add(new PlayerWarrior(2, "pepet", 300, 30));
//        players.add(new PlayerWarrior(3, "pepa", 200, 20));
//        players.add(new PlayerWarrior(4, "pep", 100, 10));
        
//        for (Player p : players) {
//        	p.setPoints(0);
//        }
        
        Comparator<Player> comp = (o1, o2) -> o1.getName().compareTo(o2.getName());
        
        System.out.println(players);
        
        players.sort(comp);
        
        System.out.println(players);
        
//        for (Player p : players) {
//            if (p.getPoints() < 200) {
//            	db.delete_player(p);
//            } else {
//                db.write_player(p);
//            }
//        }
        
//        for (Player p : players) {
//        	db.write_player(p);
//        }
        
        db.close();
    }

}
