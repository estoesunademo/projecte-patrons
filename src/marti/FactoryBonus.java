package marti;

public class FactoryBonus implements AbstractFactory<Bonus> {
	
	ObjecteEnum type;
	
	public FactoryBonus(ObjecteEnum type) {
		this.type = type;
	}
	
	@Override
	public Bonus generate(String name) {
		return new Bonus(name, type, 5);
	}
}
