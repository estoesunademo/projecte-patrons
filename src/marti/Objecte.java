package marti;

public interface Objecte {
    public String getName();
    public ObjecteEnum getType();
}
