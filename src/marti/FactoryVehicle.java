package marti;

public class FactoryVehicle implements AbstractFactory<Vehicle> {

	ObjecteEnum type;
	
	public FactoryVehicle(ObjecteEnum type) {
		this.type = type;
	}
	
	@Override
	public Vehicle generate(String name) {
		return new Vehicle(name, type);
	}
	
	
}
