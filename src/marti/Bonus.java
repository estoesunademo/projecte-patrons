package marti;

public class Bonus implements Objecte {
	private String name;
	private ObjecteEnum type;
    public int puntsASumar;

    public Bonus(String name, ObjecteEnum type, int puntsASumar) {
		super();
		this.name = name;
		this.type = type;
		this.puntsASumar = puntsASumar;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ObjecteEnum getType() {
		return type;
	}
	
	@Override
    public String toString() {
        return "Bonus [nom=" + name + ", type=" + type + ", puntsASumar=" + puntsASumar + "]";
    }

}
