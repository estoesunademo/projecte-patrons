package marti;

public class Upgrade implements Objecte {
	private String name;
	private ObjecteEnum type;
    public double factor;

    public Upgrade(String name, ObjecteEnum type, double factor) {
		super();
		this.name = name;
		this.type = type;
		this.factor = factor;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ObjecteEnum getType() {
		return type;
	}
	
	@Override
    public String toString() {
        return "Upgrade [nom=" + name + ", type=" + type + ", factor=" + factor + "]";
    }

}
