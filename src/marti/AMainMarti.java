package marti;

public class AMainMarti {
    public static void main(String[] args) {
        AFactoryProvider factory = new AFactoryProvider();

        Bonus hamburguesa = (Bonus) factory.getFactory(ObjecteEnum.HAMBURGER).generate("Big Mac");
        Collectable cofre = (Collectable) factory.getFactory(ObjecteEnum.CHEST).generate("Ender Chest");
        
        Enemy bestia = (Enemy) factory.getFactory(ObjecteEnum.BEAST).generate("Mr. Beast!");
        Upgrade laxant = (Upgrade) factory.getFactory(ObjecteEnum.LAXANT).generate("SI");
        Vehicle cotxe = (Vehicle) factory.getFactory(ObjecteEnum.CAR).generate("Volvo XC60");

        System.out.println(hamburguesa);
        System.out.println(cofre);
        System.out.println(bestia);
        System.out.println(laxant);
        System.out.println(cotxe);

    }
}
