package marti;

public class AFactoryProvider {
	public static AbstractFactory getFactory(ObjecteEnum type) {
		switch (type) {
		case HAMBURGER: case DIAMOND: case REDBULL:
			return new FactoryBonus(type);
		case PIRATE: case MONSTER: case BEAST:
			return new FactoryEnemy(type);
		case COINS: case INGOT: case CHEST:
			return new FactoryCollectable(type);
		case SWORD: case GUN: case LAXANT:
			return new FactoryUpgrade(type);
		case BOAT: case CAR: case SCOOTER: case GLIDER:
			return new FactoryVehicle(type);
		}
		return null;
	}
}
