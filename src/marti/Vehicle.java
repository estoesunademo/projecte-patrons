package marti;

public class Vehicle implements Objecte {
	private String name;
	private ObjecteEnum type;

    public Vehicle(String name, ObjecteEnum type) {
		super();
		this.name = name;
		this.type = type;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ObjecteEnum getType() {
		return type;
	}
	
	@Override
    public String toString() {
        return "Upgrade [nom=" + name + ", type=" + type + "]";
    }

}
