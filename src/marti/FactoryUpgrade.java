package marti;

public class FactoryUpgrade implements AbstractFactory<Upgrade> {

	ObjecteEnum type;
	double i;

	public FactoryUpgrade(ObjecteEnum type) {
		this.type = type;
		switch (type) {
		case LAXANT:
			i = 2;
			break;
		case SWORD:
			i = 0.25;
			break;
		case GUN:
			i = 0.5;
			break;
		default:
			i = 0;
			break;
		}
	}

	@Override
	public Upgrade generate(String name) {
		return new Upgrade(name, type, i);
	}

}