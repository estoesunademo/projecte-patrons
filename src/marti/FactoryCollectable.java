package marti;

public class FactoryCollectable implements AbstractFactory<Collectable> {

	ObjecteEnum type;
	int i;
	
	public FactoryCollectable(ObjecteEnum type) {
		this.type = type;
		switch (type) {
		case COINS:
			i = 10;
			break;
		case INGOT:
			i = 25;
			break;
		case CHEST:
			i = 50;
			break;
		default:
			i = 0;
			break;
		}
	}
	
	@Override
	public Collectable generate(String name) {
		return new Collectable(name, type, i);
	}
}
