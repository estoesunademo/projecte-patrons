package marti;

public class FactoryEnemy implements AbstractFactory<Enemy> {

	ObjecteEnum type;
	int i;
	
	public FactoryEnemy(ObjecteEnum type) {
		this.type = type;
		switch (type) {
		case PIRATE:
			i = 10;
			break;
		case MONSTER:
			i = 15;
			break;
		case BEAST:
			i = 7;
			break;
		default:
			i = 0;
			break;
		}
	}
	
	@Override
	public Enemy generate(String name) {
		return new Enemy(name, type, i);
	}
	
}