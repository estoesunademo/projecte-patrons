package marti;

public interface AbstractFactory<T> {

    public T generate(String name);

}
