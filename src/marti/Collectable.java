package marti;

public class Collectable implements Objecte {
	private String name;
	private ObjecteEnum type;
	public int puntsASumar;

    public Collectable(String name, ObjecteEnum type, int puntsASumar) {
		super();
		this.name = name;
		this.type = type;
		this.puntsASumar = puntsASumar;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ObjecteEnum getType() {
		return type;
	}
	
	@Override
    public String toString() {
        return "Bonus [nom=" + name + ", type=" + type + ", puntsASumar=" + puntsASumar + "]";
    }

}
