package marti;

public class Enemy implements Objecte {
	private String name;
	private ObjecteEnum type;
	public int puntsARestar;

	public Enemy(String name, ObjecteEnum type, int puntsARestar) {
		super();
		this.name = name;
		this.type = type;
		this.puntsARestar = puntsARestar;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ObjecteEnum getType() {
		return type;
	}

	@Override
	public String toString() {
		return "Bonus [nom=" + name + ", type=" + type + ", puntsARestar=" + puntsARestar + "]";
	}

}
