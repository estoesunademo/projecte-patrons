package comun;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import david.ColectorPunts;
import david.TipusJugador;
import ex6.DBHandler;
import manu.ColorEnum;
import manu.DecoratorColor;
import manu.DecoratorUpgrade;
import manu.DecoratorVehicle;
import manu.Player;
import manu.Player_Factory;
import manu.TypePlayer;
import marti.AFactoryProvider;
import marti.Bonus;
import marti.Collectable;
import marti.Enemy;
import marti.Objecte;
import marti.ObjecteEnum;
import marti.Upgrade;
import marti.Vehicle;

public class Main {
	public static void main(String[] args) {
		
//		1 - Crea els factorys que necessitis per crear objectes.
		AFactoryProvider o_factory = new AFactoryProvider();
		Player_Factory p_factory = new Player_Factory();
		
//		2 - Crea els jugadors  i es recomana afegir algun objecte a millorar.
//		 Aquests jugadors, els has de posar en una cua. Després els afegeixes a la BD si no exiteixen. Almenys crea 3 jugadors.
		Queue<Player> playerList = new LinkedList<Player>();
		
		Player manu = p_factory.createPlayer(TypePlayer.NOBLE, 1, "Manu", 0);
		Player david =  p_factory.createPlayer(TypePlayer.MERCHANT, 2, "David", 0);
		Player marti = p_factory.createPlayer(TypePlayer.WARRIOR, 3, "Marti", 0);
		
		playerList.add(david);		
		playerList.add(marti);
		playerList.add(manu);
		
		TipusJugador t_manu = new TipusJugador(manu);
		TipusJugador t_david = new TipusJugador(david);
		TipusJugador t_marti = new TipusJugador(marti);
		
//		Upgrade gun = (Upgrade) o_factory.getFactory(ObjecteEnum.GUN).generate("PISTI");
//		t_manu.setUpgrade(gun);

//		Upgrade l = (Upgrade) o_factory.getFactory(ObjecteEnum.LAXANT).generate("supositori");
//		t_marti.setUpgrade(l);
//		t_david.setUpgrade(l);
		
		String db_host = "localhost";
        String db_name = "videojocjdbc";
        String db_user = "root";
        String db_pass = "super3";
        
        DBHandler db = new DBHandler(db_host, db_name, db_user, db_pass);
        db.connect();
        
        for (Player player : playerList) {
			db.write_player(player);
		}
        
        
//        3 - Crea els diferents objectes i els poses en una llista. 
//        Almenys crea un de cada classe. Per exemple, crea la llista dels 
//        enemics i crea un monstre, pirata i bèstia i el poses dintre de la 
//        llista. Així amb tots, inclosos els colors.

        List<Objecte> objs = new ArrayList<Objecte>();
                
        Enemy bestia = (Enemy) o_factory.getFactory(ObjecteEnum.BEAST).generate("Mr. Beast!");
        Enemy pirata = (Enemy) o_factory.getFactory(ObjecteEnum.PIRATE).generate("Jack Sparrago");
        Enemy monstre = (Enemy) o_factory.getFactory(ObjecteEnum.MONSTER).generate("Montre Boo");

        objs.add(bestia);
        objs.add(pirata);
        objs.add(monstre);
        
        
        
//      4 - Crea l’objecte observable i subscriu tots els jugadors.
        ColectorPunts colectorPunts = new ColectorPunts();
		
		colectorPunts.addPropertyChangeListener(t_manu);
		colectorPunts.addPropertyChangeListener(t_david);
		colectorPunts.addPropertyChangeListener(t_marti);
		
//		5 - Crea un bucle de n repeticions. Es recomana almenys 50 repeticions. Després dintre del bucle:
		int n = 150;
		Random r = new Random();
		for (int i = 0; i < n; i++) {
			int dice = r.nextInt(1,7);
			System.out.println("Tiramos dado: "+ dice);
			System.out.println("orden actual lista jugadores " + playerList);
			for (int j = 0; j < dice; j++) {
				Player p = playerList.poll();
				System.out.println("cogemos un jugador..." + p.getName() +" y lo ponemos al final");
				playerList.offer(p);
			}
			Player p = playerList.poll();
			System.out.println(p.getName());
			
//			C - A partir d’aquest valor aleatori, es decideix si l’acció és caçar un objecte, 
//			obtenir un bonus, t’ha tocat un enemic o afegeixes un decorador com un color, 
//			vehicle o objecte a millorar. -- nosotros hacemos entre 4 el random --
			int val = r.nextInt(1,5);
			switch(val) {
			case 1:
		        Collectable c = (Collectable) o_factory.getFactory(ObjecteEnum.CHEST).generate("Mr. Beast Chest");
		        colectorPunts.findObject(p, c);
		        System.out.println(p.getName() + " wins collectable!");
				playerList.offer(p);
		        break;
			case 2:
		        Bonus b = (Bonus) o_factory.getFactory(ObjecteEnum.HAMBURGER).generate("Mr. Beast Burguer");
		        colectorPunts.findObject(p, b);
		        System.out.println(p.getName() + " wins bonus!");
				playerList.offer(p);
				break;
			case 3:
				Enemy e = (Enemy) o_factory.getFactory(ObjecteEnum.BEAST).generate("Mr. Beast Himself");
		        colectorPunts.findObject(p, e);
		        System.out.println(p.getName() + " gets enemy!");
				playerList.offer(p);
				break;
			case 4:
				switch(r.nextInt(3)) {
				case 0:
					p = new DecoratorColor(p, ColorEnum.HOTPINK);
					System.out.println(((DecoratorColor) p).getPlayerToDecorate().getName() + " is now a decorator color!");
					playerList.offer(((DecoratorColor) p).getPlayerToDecorate());
					break;
				case 1:
					p = new DecoratorVehicle(p, (Vehicle) o_factory.getFactory(ObjecteEnum.CAR).generate("Mr. Beast Tesla"));
					System.out.println(((DecoratorVehicle) p).getPlayerToDecorate().getName() + " is now a decorator vehicle!");
					playerList.offer(((DecoratorVehicle) p).getPlayerToDecorate());
					break;
				case 2:
					p = new DecoratorUpgrade(p,  (Upgrade) o_factory.getFactory(ObjecteEnum.LAXANT).generate("Mr. Beast Prize"));
					System.out.println(((DecoratorUpgrade) p).getPlayerToDecorate().getName() + " is now a decorator upgrade!");
					playerList.offer(((DecoratorUpgrade) p).getPlayerToDecorate());
					break;
				}
				break;
			}
		}
        
//		6 - Un cop finalitzada la simulació, heu de treure del observable els jugadors.
		colectorPunts.removePropertyChangeListener(t_manu);
		colectorPunts.removePropertyChangeListener(t_david);
		colectorPunts.removePropertyChangeListener(t_marti);
		System.out.println("IMPRIME TODOS: ");
        for (Player player : playerList) {
			System.out.println(player);
		}
        
//      7 - Actualitzar la seva puntuació. Si els jugadors no arriba a 500 punts s’esborra de la BD. En cas contrari, s’actualitzen els punts.
        System.out.println("ACTUALIZA BBDD ");
		for (Player p : playerList) {
			if (p.getPoints() < 100) {
				db.delete_player(p);
			} else {
				db.write_player(p);
			}
		}
		
//		8 - Un cop actualitzada la BD, recupera la llista de jugadors i mostra-la per consola ordenada alfabèticament 
//		i després per puntuació. Necessitaràs un comparable i un comparator.
		 System.out.println();	
		System.out.println("LEE Y ORDENA DE BBDD POR STRING NOM");
		List<Player> db_players = db.get_players();
		Collections.sort(db_players);		
       
        for (Player player : db_players) {
			System.out.println(player);
		}
        
        System.out.println();
        System.out.println("ORDENA POR PUNTOS DE BBDD: ");
        Comparator<Player> comp = (o1, o2) -> o1.getPoints() - o2.getPoints();
        db_players.sort(comp);
        
        System.out.println();
        
        for (Player player : db_players) {
			System.out.println(player);
		}
        
        db.close();
	}
}
