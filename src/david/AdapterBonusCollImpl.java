package david;

import marti.Bonus;
import marti.Collectable;
import marti.Objecte;
import marti.Upgrade;

public class AdapterBonusCollImpl implements AdapterBonus {

	Bonus bonus;
	Collectable collectable;
	Upgrade upgrade = null;

	public AdapterBonusCollImpl(Collectable collectable, Upgrade upgrade) {
		this.collectable = collectable;
		this.upgrade = upgrade;
	}

	public AdapterBonusCollImpl(Bonus bonus, Upgrade upgrade) {
		this.bonus = bonus;
		this.upgrade = upgrade;
	}

	@Override
	public int getPunts(Objecte o) {
		int points = 0;
		if (o instanceof Bonus) {
			points = +((Bonus) o).puntsASumar;
			if (upgrade != null)
				points /= upgrade.factor;
		} else if (o instanceof Collectable) {
			points = +((Collectable) o).puntsASumar;
			if (upgrade != null)
				points /= upgrade.factor;
		}
		return points;

	}

}
