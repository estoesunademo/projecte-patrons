package david;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import manu.Player;
import marti.Bonus;
import marti.Collectable;
import marti.Enemy;
import marti.Objecte;
import marti.Upgrade;

public class TipusJugador implements PropertyChangeListener {

	Upgrade upgrade;
	Player player;

	int points;

	public TipusJugador(Player player) {
		super();
		this.player = player;
		this.points = 0;
		this.upgrade = null;

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (!(evt.getNewValue() instanceof Message)) {
			return;
		}
		Message msg = (Message) evt.getNewValue();
		if (msg.player == this.player) {
			setPoints(msg.obj);
		}
	}

	public void setPoints(Objecte o) {
		int points = 0;
		if (o instanceof Bonus) {
			AdapterBonusCollImpl adapBon = new AdapterBonusCollImpl((Bonus) o, upgrade);
			points = adapBon.getPunts(o);
		} else if (o instanceof Collectable) {
			AdapterBonusCollImpl adapCol = new AdapterBonusCollImpl((Collectable) o, upgrade);
			points = adapCol.getPunts(o);
		} else if (o instanceof Enemy) {
			AdapterEnemicImpl adapEne = new AdapterEnemicImpl((Enemy) o, upgrade);
			points = adapEne.getPunts(o);
		}

		this.player.setPoints(this.player.getPoints() + points);

	}

	public void setUpgrade(Upgrade upgrade) {
		this.upgrade = upgrade;

	}
}