package david;

import marti.Enemy;
import marti.Objecte;
import marti.Upgrade;

public class AdapterEnemicImpl implements AdapterEnemic {

	Enemy enemy;
	Upgrade upgrade;

	public AdapterEnemicImpl(Enemy e, Upgrade upgrade) {
		this.enemy = e;
		this.upgrade = upgrade;
	}

	@Override
	public int getPunts(Objecte o) {
		int points = 0;

		if (o instanceof Enemy) {
			points = -((Enemy) o).puntsARestar;
			if (upgrade != null)
				points *= upgrade.factor;
		}
		return points;
	}
}
