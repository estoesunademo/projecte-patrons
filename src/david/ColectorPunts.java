package david;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import manu.Player;
import marti.Objecte;

public class ColectorPunts {

	int oldPoints;
	private PropertyChangeSupport support;

	public ColectorPunts() {
		super();
		this.oldPoints = 0;
		this.support = new PropertyChangeSupport(this);
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		support.removePropertyChangeListener(pcl);
	}

	public void findObject(Player player, Objecte objecte) {
		Message msg = new Message(player, objecte);
		System.out.println(msg.toString());
		support.firePropertyChange("", "", msg);
	}

}
