package david;

import manu.Player;
import marti.Objecte;

public class Message {

	Player player;
	Objecte obj;

	public Message(Player player, Objecte obj) {
		super();
		this.player = player;
		this.obj = obj;
	}

	@Override
	public String toString() {
		return "Message [player=" + player + ", obj=" + obj + "]";
	}

}
