package david;

import java.util.ArrayDeque;
import java.util.Deque;

import manu.Player;
import manu.Player_Factory;
import manu.TypePlayer;
import marti.AFactoryProvider;
import marti.Bonus;
import marti.Enemy;
import marti.ObjecteEnum;
import marti.Upgrade;

public class MainDavid {

	@SuppressWarnings("static-access")
	public static void main(String[] args) {

		// ----------------ACT4------------------//

		AFactoryProvider factory = new AFactoryProvider();
//		Utilitza Factory Method per crear els jugadors de l’aplicació
		Player_Factory factory2 = new Player_Factory();

//		Aquest jugadors s’han de guardar en una pila.
		Deque<Player> playerList = new ArrayDeque<>();

		Player pl01 = factory2.createPlayer(TypePlayer.WARRIOR, 1, "Goliath", 0, playerList);
		Player pl02 = factory2.createPlayer(TypePlayer.MERCHANT, 2, "Kitsune", 0, playerList);
		Player pl03 = factory2.createPlayer(TypePlayer.NOBLE, 3, "Griffith", 0, playerList);

//		Mostra per pantalla la creació dels jugadors. 
		for (Player player : playerList) {
			System.out.println(player);
		}

		System.out.println(pl01.getPoints());
		Bonus hamburguesa = (Bonus) factory.getFactory(ObjecteEnum.HAMBURGER).generate("Big Mac");
		Enemy milBicis = (Enemy) factory.getFactory(ObjecteEnum.PIRATE).generate("Torrubiano");
		Upgrade laxante = (Upgrade) factory.getFactory(ObjecteEnum.LAXANT).generate("Torrubiano");

		TipusJugador tipl01 = new TipusJugador(pl01);
		TipusJugador tipl02 = new TipusJugador(pl02);
		
		ColectorPunts colectorPunts = new ColectorPunts();
		
		colectorPunts.addPropertyChangeListener(tipl01);
		
		colectorPunts.findObject(pl01, hamburguesa);
		colectorPunts.findObject(pl02, laxante);
		System.out.println(pl01.getPoints());

		// ----------------ACT5 ADAPTER------------------//

		colectorPunts.addPropertyChangeListener(tipl02);
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println(pl02.getPoints());
		colectorPunts.findObject(pl02, hamburguesa);
		System.out.println(pl02.getPoints());
		tipl02.setUpgrade(laxante);
		System.out.println(pl02.getPoints());
		colectorPunts.findObject(pl02, hamburguesa);
		System.out.println(pl02.getPoints());

	}

}