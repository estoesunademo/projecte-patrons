package david;

import manu.Player;
import marti.Bonus;
import marti.Collectable;
import marti.Enemy;
import marti.Objecte;

public class TipusJugadorAdapterImplementacio implements TipusJugadorAdapter {

	private TipusJugador tipusJugador;

	public TipusJugadorAdapterImplementacio(TipusJugador tipusJugador) {
		this.tipusJugador = tipusJugador;
	}

	@Override
	public void setPoints(Player p, Objecte o) {
		int points = 0;
		if (o instanceof Bonus) {
			points = ((Bonus) o).puntsASumar;

		} else if (o instanceof Collectable) {
			points = ((Collectable) o).puntsASumar;

		} else if (o instanceof Enemy) {
			points = -((Enemy) o).puntsARestar;
		}
		tipusJugador.points += points;
	}
}
